package StudentManager

import javafx.scene.control.TextField
import tornadofx.*
import kotlin.jdbc.BD

/**
 * Created by idib on 02.06.17.
 */

class TypeDoc(base: BD, id: Int, name: String) : Model() {
    override val id: Int by property(id)
    var name: String by property(name)
    val base: BD by property(base)

    companion object {
        fun nullTypeDoc(b: BD): TypeDoc {
            return TypeDoc(b, -1, "")
        }
    }

    fun nameProperty() = getProperty(Faculty::name)

    override fun CeateUpdate() {
        if (id == -1)
            base.InsertTypeDoc(this)
        else
            base.UpdateTypeDoc(this)
    }

    override fun Delete() {
        base.DeleteTypeDoc(this)
    }

    fun OpenEditor(con: Controllers) {
        val editScope = TypeDocScope(this, con)
        return find(TypeDocEditor::class, editScope).openWindow()
    }
}


class TypeDocScope(var curModel: TypeDoc, var con: Controllers) : Scope()

class TypeDocEditor() : View() {
    var nameField: TextField by singleAssign()
    val t = this
    val cmodel = (super.scope as TypeDocScope).curModel
    val con = (super.scope as TypeDocScope).con
    val obsList = con.stud

    override val root = form {
        fieldset("Тип Документа") {
            field("Название") {
                nameField = textfield {
                    bind(cmodel.nameProperty())
                }
            }
            children.addAll(SaveCanelDelete(cmodel, con, t))
        }
    }
}
