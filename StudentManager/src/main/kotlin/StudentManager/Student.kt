package StudentManager

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.scene.Parent
import javafx.scene.control.CheckBox
import javafx.scene.control.DatePicker
import javafx.scene.control.TextField
import loginMenager.OpenUnitUni
import tornadofx.*
import java.time.LocalDate
import kotlin.jdbc.BD


/**
 * Created by idib on 16.05.17.
 */

class Student(base: BD, id: Int, surname: String, name: String, patronymic: String,
              adress: String, phone: String, dorm: Boolean, dateEnrolment: LocalDate,
              isn: Boolean, dateRemand: LocalDate?, idGroup: Int, group: String, treand: String,
              faculty: String) : Model() {

    var base: BD = base
    override var id: Int by property(id)
    var surname: String by property(surname)
    var name: String by property(name)
    var patronymic: String by property(patronymic)
    var adress: String by property(adress)
    var phone: String by property(phone)
    var dorm: Boolean by property(dorm)
    var dateEnrolment: LocalDate by property(dateEnrolment)
    var dateRemand: LocalDate? by property(dateRemand)
    var idGroup: Int by property(idGroup)
    var group: String by property(group)
    var treand: String by property(treand)
    var faculty: String by property(faculty)
    var useDate: Boolean by property(isn)
    var nnul by property("null")

    companion object {
        fun nullStud(b: BD): Student {
            return Student(b, -1, "", "", "", "", "", true, LocalDate.now(), 0, "", "", "")
        }
    }


    constructor(base: BD, id: Int, surname: String, name: String, patronymic: String,
                adress: String, phone: String, dorm: Boolean, dateEnrolment: LocalDate,
                dateRemand: LocalDate?, idGroup: Int, group: String, treand: String,
                faculty: String) : this(base, id, surname, name, patronymic, adress,
            phone, dorm, dateEnrolment, true, dateRemand, idGroup, group, treand,
            faculty)

    constructor(base: BD, id: Int, surname: String, name: String, patronymic: String,
                adress: String, phone: String, dorm: Boolean, dateEnrolment: LocalDate,
                idGroup: Int, group: String, treand: String,
                faculty: String) : this(base, id, surname, name, patronymic, adress,
            phone, dorm, dateEnrolment, false, null, idGroup, group, treand,
            faculty)

    constructor(stud: Student) : this(stud.base, stud.id, stud.surname, stud.name, stud.patronymic, stud.adress,
            stud.phone, stud.dorm, stud.dateEnrolment, stud.useDate, stud.dateRemand, stud.idGroup, stud.group, stud.treand,
            stud.faculty)


    fun surnameProperty() = getProperty(Student::surname)
    fun nameProperty() = getProperty(Student::name)
    fun patronymicProperty() = getProperty(Student::patronymic)
    fun adressProperty() = getProperty(Student::adress)
    fun phoneProperty() = getProperty(Student::phone)
    fun dormProperty() = getProperty(Student::dorm)
    fun dateEnrolmentProperty() = getProperty(Student::dateEnrolment)
    fun dateRemandProperty() = if (useDate)
        getProperty(Student::dateRemand)
    else
        getProperty(Student::nnul)

    fun dateRemandProperty2() = getProperty(Student::dateRemand)
    fun groupProperty() = getProperty(Student::group)
    fun treandProperty() = getProperty(Student::treand)
    fun facultyProperty() = getProperty(Student::faculty)
    fun nnulProperty() = getProperty(Student::useDate)
    fun NameSur() = surname + " " + name

    override fun CeateUpdate() {
        if (id == -1)
            base.InsertStudent(this)
        else
            base.UpdateStudents(this)
    }

    override fun Delete() {
        base.DeleteStudent(this)
    }

    fun OpenEditor(con: Controllers) {
        val editScope = StudScope(this, con)
        return find(StudentEditor::class, editScope).openWindow()
    }
}


class StudScope(var curModel: Student, var con: Controllers) : Scope()

class StudentEditor() : View() {
    var nameField: TextField by singleAssign()
    var surnameField: TextField by singleAssign()
    var patronymicField: TextField by singleAssign()
    var adressField: TextField by singleAssign()
    var phoneField: TextField by singleAssign()
    var dormField: CheckBox by singleAssign()
    var dateEnrolmentField: DatePicker by singleAssign()
    var dateRemandField: DatePicker by singleAssign()
    var groupField: TextField by singleAssign()
    var treandField: TextField by singleAssign()
    var facultyField: TextField by singleAssign()

    var usableDate: CheckBox by singleAssign()

    val cmodel = (super.scope as StudScope).curModel
    val con = (super.scope as StudScope).con

    override val root = form {
        fieldset("Студент", FontAwesomeIconView(FontAwesomeIcon.USER)) {
            field("Фамилия") {
                surnameField = textfield {
                    bind(cmodel.surnameProperty())
                }
            }
            field("Имя") {
                nameField = textfield {
                    bind(cmodel.nameProperty())
                }
            }
            field("Отчество") {
                patronymicField = textfield {
                    bind(cmodel.patronymicProperty())
                }
            }
            field("Адресс") {
                adressField = textfield {
                    bind(cmodel.adressProperty())
                }
            }
            field("Телефон") {
                phoneField = textfield {
                    bind(cmodel.phoneProperty())
                }
            }
            field("Проживает в общажитии") {
                dormField = checkbox {
                    bind(cmodel.dormProperty())
                }

            }
            field("Дата зачисления") {
                dateEnrolmentField = datepicker() {
                    bind(cmodel.dateEnrolmentProperty())
                }
            }

            children.addAll(NullDate("Дата отчисления", cmodel.nnulProperty(), cmodel.dateRemandProperty2()))

            field("Група") {
                val tex = label(cmodel.group + " " + cmodel.treand + " " + cmodel.faculty)
                button("Изменить") {
                    setOnAction {
                        val editScope = GScope(cmodel, tex)
                        var own = find(GroupView::class, editScope).openWindow()
                    }
                }
            }

            field("Связанные документы") {
                button("Документы") {
                    setOnAction {
                        OpenUnitUni(doctab(cmodel.base, con, cmodel))
                    }
                }
                button("Приказы") {
                    setOnAction {
                        OpenUnitUni(decreeTWO(cmodel.base, con, cmodel))
                    }
                }
            }

            field("Родители") {
                button("Посмотреть Изменить") {
                    setOnAction {
                        OpenUnitUni(parentTWO(cmodel.base, con, cmodel))
                    }
                }
            }

            children.addAll(SaveCanelDelete(cmodel, con, this@StudentEditor))
        }
    }
}
