package StudentManager

import javafx.beans.value.ChangeListener
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.control.ComboBox
import javafx.scene.control.TextField
import tornadofx.*
import kotlin.jdbc.BD

/**
 * Created by idib on 31.05.17.
 */
class Treand(base: BD, id: Int, name: String, idFaculty: Int, faculty: String) : Model() {
    override val id: Int by property(id)
    var name: String by property(name)
    var faculty: String by property(faculty)
    val base: BD by property(base)

    var idFaculty: Int by property(idFaculty)

    fun nameProperty() = getProperty(Treand::name)
    fun facultyProperty() = getProperty(Treand::faculty)

    companion object {
        fun nullTreand(b: BD): Treand {
            return Treand(b, -1, "", -1, "")
        }
    }

    override fun CeateUpdate() {
        if (id == -1)
            base.InsertTreand(this)
        else
            base.UpdateTreand(this)
    }

    override fun Delete() {
        base.DeleteTreand(this)
    }


    fun OpenEditor(con: Controllers) {
        val editScope = TreandScope(this, con)
        return find(TreandEditor::class, editScope).openWindow()
    }
}

class TreandScope(var curModel: Treand, var con: Controllers) : Scope()

class TreandEditor() : View() {
    var nameField: TextField by singleAssign()
    var facultyField: TextField by singleAssign()
    val t = this
    val cmodel = (super.scope as TreandScope).curModel
    val con = (super.scope as TreandScope).con

    var c1: ComboBox<String> by singleAssign()


    var Fid: List<Int> = ArrayList()
    var Flist: ObservableList<String> = FXCollections.observableArrayList()

    init {
        cmodel.base.selectFacultys(Fid, Flist)
    }

    override val root = form {
        fieldset("Направление") {
            field("Название") {
                nameField = textfield {
                    bind(cmodel.nameProperty())
                }
            }
            field("Факультет") {
                c1 = combobox(values = Flist) {
                    selectionModel.select(Fid.indexOf(cmodel.idFaculty))
                    valueProperty().addListener(ChangeListener { observable, oldValue, newValue ->
                        if (newValue != null && Flist.contains(newValue)) {
                            cmodel.idFaculty = Fid[Flist.indexOf(newValue)]
                            cmodel.faculty = c1.selectionModel.selectedItem
                        }
                    })
                }
            }

            children.addAll(SaveCanelDelete(cmodel, con, t))
        }
    }
}
