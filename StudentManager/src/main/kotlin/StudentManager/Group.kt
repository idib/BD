package StudentManager

import javafx.beans.value.ChangeListener
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.control.ComboBox
import javafx.scene.control.TextField
import tornadofx.*
import kotlin.jdbc.BD

/**
 * Created by idib on 31.05.17.
 */

class Group(base: BD, id: Int, name: String, idTreand: Int, treand: String, faculty: String) : Model() {
    override val id: Int by property(id)
    var name: String by property(name)
    var treand: String by property(treand)
    var faculty: String by property(faculty)
    val base: BD by property(base)

    var idTreand: Int by property(idTreand)

    fun nameProperty() = getProperty(Group::name)
    fun facultyProperty() = getProperty(Group::faculty)

    companion object {
        fun nullGroup(b: BD): Group {
            return Group(b, -1, "",-1,"","")
        }
    }

    override fun CeateUpdate() {
        if (id == -1)
            base.InsertGroup(this)
        else
            base.UpdateGroup(this)
    }

    override fun Delete() {
        base.DeleteGroup(this)
    }

    fun OpenEditor(con: Controllers) {
        val editScope = GroupScope(this, con)
        return find(GroupEditor::class, editScope).openWindow()
    }
}



class GroupScope(var curModel: Group, var con: Controllers) : Scope()

class GroupEditor() : View() {
    var nameField: TextField by singleAssign()
    var facultyField: TextField by singleAssign()
    val t = this
    val cmodel = (super.scope as GroupScope).curModel
    val con = (super.scope as GroupScope).con
    val obsList = con.group

    var c1: ComboBox<String> by singleAssign()
    var c2: ComboBox<String> by singleAssign()

    var gm:GroupManager = GroupManager(cmodel.id,cmodel.base)

    override val root = form {
        fieldset("Студент") {
            field("Название") {
                nameField = textfield {
                    bind(cmodel.nameProperty())
                }
            }
            field("Факультет") {
                c1 = combobox(values = gm.Flist) {
                    selectionModel.select(gm.Fid.indexOf(cmodel.idTreand))
                    valueProperty().addListener(ChangeListener { observable, oldValue, newValue ->
                        if (newValue != null && gm.Flist.contains(newValue)) {
                            gm.SelectF(gm.Fid[gm.Flist.indexOf(newValue)])
                            c2.selectionModel.select("")
                        }
                    })
                }
            }
            field("Поток") {
                c2 = combobox(values = gm.Tlist) {
                    selectionModel.select(gm.Tid.indexOf(gm.t))
                    valueProperty().addListener(ChangeListener { observable, oldValue, newValue ->
                        if (newValue != null && gm.Tlist.contains(newValue)) {
                            cmodel.idTreand =  gm.Tid[gm.Tlist.indexOf(newValue)]
                        }
                    })
                }
            }
            children.addAll(SaveCanelDelete(cmodel, con, t))
        }
    }
}
