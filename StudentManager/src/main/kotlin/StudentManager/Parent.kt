package ParentManager

import StudentManager.Controllers
import StudentManager.Model
import StudentManager.SaveCanelDelete
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.scene.control.CheckBox
import javafx.scene.control.DatePicker
import javafx.scene.control.TextField
import loginMenager.OpenUnitUni
import tornadofx.*
import java.time.LocalDate
import kotlin.jdbc.BD


/**
 * Created by idib on 16.05.17.
 */

class Parent(base: BD, id: Int, surname: String, name: String, patronymic: String,
             adress: String, phone: String) : Model() {

    var base: BD = base
    override var id: Int by property(id)
    var surname: String by property(surname)
    var name: String by property(name)
    var patronymic: String by property(patronymic)
    var adress: String by property(adress)
    var phone: String by property(phone)

    companion object {
        fun nullParent(b: BD): Parent {
            return Parent(b, -1, "", "", "", "", "")
        }
    }


    fun surnameProperty() = getProperty(Parent::surname)
    fun nameProperty() = getProperty(Parent::name)
    fun patronymicProperty() = getProperty(Parent::patronymic)
    fun adressProperty() = getProperty(Parent::adress)
    fun phoneProperty() = getProperty(Parent::phone)
    fun NameSur() = surname + " " + name

    override fun CeateUpdate() {
        if (id == -1)
            base.InsertParent(this)
        else
            base.UpdateParent(this)
    }

    override fun Delete() {
        base.DeleteParent(this)
    }

    fun OpenEditor(con: Controllers) {
        val editScope = ParentScope(this, con)
        return find(ParentEditor::class, editScope).openWindow()
    }
}


class ParentScope(var curModel: Parent, var con: Controllers) : Scope()

class ParentEditor() : View() {
    var nameField: TextField by singleAssign()
    var surnameField: TextField by singleAssign()
    var patronymicField: TextField by singleAssign()
    var adressField: TextField by singleAssign()
    var phoneField: TextField by singleAssign()
    var dormField: CheckBox by singleAssign()
    var dateEnrolmentField: DatePicker by singleAssign()
    var dateRemandField: DatePicker by singleAssign()
    var groupField: TextField by singleAssign()
    var treandField: TextField by singleAssign()
    var facultyField: TextField by singleAssign()

    var usableDate: CheckBox by singleAssign()

    val cmodel = (super.scope as ParentScope).curModel
    val con = (super.scope as ParentScope).con

    override val root = form {
        fieldset("Студент", FontAwesomeIconView(FontAwesomeIcon.USER)) {
            field("Фамилия") {
                surnameField = textfield {
                    bind(cmodel.surnameProperty())
                }
            }
            field("Имя") {
                nameField = textfield {
                    bind(cmodel.nameProperty())
                }
            }
            field("Отчество") {
                patronymicField = textfield {
                    bind(cmodel.patronymicProperty())
                }
            }
            field("Адресс") {
                adressField = textfield {
                    bind(cmodel.adressProperty())
                }
            }
            field("Телефон") {
                phoneField = textfield {
                    bind(cmodel.phoneProperty())
                }
            }
            field("Дети") {
                button("Посмотреть") {
                    setOnAction {
                        //todo
                    }
                }
            }

            children.addAll(SaveCanelDelete(cmodel, con, this@ParentEditor))
        }
    }
}
