package StudentManager

import javafx.scene.layout.Pane
import tornadofx.Controller
import tornadofx.View
import tornadofx.button
import tornadofx.hbox

/**
 * Created by idib on 31.05.17.
 */

class SaveCanelDelete(cmodel: Model, obsList: Controllers, view: View) : Pane() {

    val root = hbox {
        button("Сохранить") {
            setOnAction {
                cmodel.CeateUpdate()
                obsList.update()
                view.close()
            }
        }
        button("Отмена") {
            setOnAction {
                obsList.update()
                view.close()
            }
        }
        if (cmodel.id != -1) {
            button("Удалить") {
                setOnAction {
                    cmodel.Delete()
                    obsList.update()
                    view.close()
                }
            }
        }
    }
}


abstract class Model : Controller() {
    abstract val id: Int
    abstract fun CeateUpdate()
    abstract fun Delete()
}
