package StudentManager

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.beans.value.ChangeListener
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.control.ComboBox
import javafx.scene.control.Label
import tornadofx.*
import kotlin.jdbc.BD

/**
 * Created by idib on 25.05.17.
 */


class GroupManager(idGroup: Int, base: BD) {
    var b = base
    var g: Int = idGroup
    var Gid: List<Int> = ArrayList()
    var Glist: ObservableList<String> = FXCollections.observableArrayList()

    var t: Int = 0
    var Tid: List<Int> = ArrayList()
    var Tlist: ObservableList<String> = FXCollections.observableArrayList()

    var f: Int = 0
    var Fid: List<Int> = ArrayList()
    var Flist: ObservableList<String> = FXCollections.observableArrayList()

    init {
        if (g != -1) {
            t = getTid()
            f = getFid()
        }
        getF()
    }

    fun getTid(): Int {
        return b.getTid(g, Gid, Glist)
    }

    fun getFid(): Int {
        return b.getFid(t, Tid, Tlist)
    }

    fun getF() {
        b.selectFacultys(Fid, Flist)
    }

    fun getT() {
        b.getTreand(f, Tid, Tlist)
    }

    fun getG() {
        b.getGroup(t, Gid, Glist)
    }

    fun SelectF(i: Int) {
        f = i
        getT()
    }

    fun SelectT(i: Int) {
        t = i
        getG()
    }
}

class GScope : Scope {
    val curModel: Student
    val tex: Label

    constructor(model: Student, t: Label) {
        this.curModel = model
        this.tex = t
    }
}

class GroupView() : View() {

    var cmodel = (super.scope as GScope).curModel
    var tex = (super.scope as GScope).tex
    var gm = GroupManager(cmodel.idGroup, cmodel.base)

    var c1: ComboBox<String> by singleAssign()
    var c2: ComboBox<String> by singleAssign()
    var c3: ComboBox<String> by singleAssign()

    override val root = form {
        fieldset("Выбор групы", FontAwesomeIconView(FontAwesomeIcon.GROUP)) {
            field("Факультет") {
                c1 = combobox(values = gm.Flist) {
                    selectionModel.select(gm.Fid.indexOf(gm.f))
                    valueProperty().addListener(ChangeListener { observable, oldValue, newValue ->
                        if (newValue != null && gm.Flist.contains(newValue)) {
                            gm.SelectF(gm.Fid[gm.Flist.indexOf(newValue)])
                            c2.selectionModel.select("")
                            c3.selectionModel.select("")
                        }
                    })
                }
            }

            field("Поток") {
                c2 = combobox(values = gm.Tlist) {
                    selectionModel.select(gm.Tid.indexOf(gm.t))
                    valueProperty().addListener(ChangeListener { observable, oldValue, newValue ->
                        if (newValue != null && gm.Tlist.contains(newValue)) {
                            gm.SelectT(gm.Tid[gm.Tlist.indexOf(newValue)])
                            c3.selectionModel.select("")
                        }
                    })
                }
            }

            field("Група") {
                c3 = combobox(values = gm.Glist) {
                    selectionModel.select(gm.Gid.indexOf(gm.g))
                    valueProperty().addListener(ChangeListener { observable, oldValue, newValue ->
                        if (newValue != null && gm.Glist.contains(newValue)) {
                            gm.g = gm.Gid[gm.Glist.indexOf(newValue)]
                        }
                    })
                }
            }

        }

        hbox {
            button("Сохранить") {
                setOnAction {
                    if (c3.value != null && c3.value != "") {
                        cmodel.idGroup = gm.g
                        cmodel.group = c3.selectionModel.selectedItem
                        cmodel.treand = c2.selectionModel.selectedItem
                        cmodel.faculty = c1.selectionModel.selectedItem
                        tex.text = cmodel.group + " " + cmodel.treand + " " + cmodel.faculty
                        close()
                    }else
                    {
                        //todo вывести предупреждение
                    }
                }
            }
            button("Отмена") {
                setOnAction {
                    close()
                }
            }
        }
    }
}