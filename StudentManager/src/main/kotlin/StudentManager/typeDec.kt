package StudentManager

import javafx.scene.control.TextField
import tornadofx.*
import kotlin.jdbc.BD

/**
 * Created by idib on 02.06.17.
 */

class TypeDec(base: BD, id: Int, name: String) : Model() {
    override val id: Int by property(id)
    var name: String by property(name)
    val base: BD by property(base)

    companion object {
        fun nullTypeDoc(b: BD): TypeDoc {
            return TypeDoc(b, -1, "")
        }
    }

    fun nameProperty() = getProperty(Faculty::name)

    override fun CeateUpdate() {
        if (id == -1)
            base.InsertTypeDecree(this)
        else
            base.UpdateTypeDecree(this)
    }

    override fun Delete() {
        base.DeleteTypeDecree(this)
    }

    fun OpenEditor(con: Controllers) {
        val editScope = TypeDecScope(this, con)
        return find(TypeDecEditor::class, editScope).openWindow()
    }
}

class TypeDecScope(var curModel: TypeDec, var con: Controllers) : Scope()

class TypeDecEditor() : View() {
    var nameField: TextField by singleAssign()
    val t = this
    val cmodel = (super.scope as TypeDecScope).curModel
    val con = (super.scope as TypeDecScope).con
    val obsList = con.stud

    override val root = form {
        fieldset("Тип Приказа") {
            field("Название") {
                nameField = textfield {
                    bind(cmodel.nameProperty())
                }
            }
            children.addAll(SaveCanelDelete(cmodel, con, t))
        }
    }
}
