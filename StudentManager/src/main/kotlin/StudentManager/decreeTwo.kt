package StudentManager

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.scene.control.ComboBox
import javafx.scene.control.TableView
import tornadofx.*
import kotlin.jdbc.BD

/**
 * Created by idib on 01.06.17.
 */

class decreeTWO(var base: BD, var con: Controllers, var stud: StudentController) : Fragment() {
    var sor = stud.getNames()
    var names = sor.second
    var ids = sor.first
    var default: String = ""
    var decreeController = DecreeController(base)
    var decreeController2 = DecreeController(base)
//    var c1: ComboBox<String> by singleAssign()
    var student: Student = Student.nullStud(base)
    var t1: TableView<Decree> by singleAssign()
    var t2: TableView<Decree> by singleAssign()

    constructor(base: BD, con: Controllers) : this(base, con, con.stud) {
        stud.update()
        sor = stud.getNames()
        names = sor.second
        ids = sor.first
    }


    constructor(base: BD, con: Controllers, student: Student) : this(base, con, StudentController(base)) {
        stud.update(student.id)
        sor = stud.getNames()
        names = sor.second
        ids = sor.first
//        c1.selectionModel.select(student.NameSur())
        this.student = student
        decreeController.ii = student.id
        decreeController2.dd = student.id
        decreeController.update()
        decreeController2.update()
    }

    override val root =
            vbox {
                hbox {
                    button("Добавить новую запись", FontAwesomeIconView(FontAwesomeIcon.PLUS)) {
                        setOnAction {
                            var d = Decree.nullDoc(base)
//                            if (student.id != -1)         todo
//                                d.idStud = student.id
                            var own = d.OpenEditor(con)
                        }
                    }
                    button("Обновить", FontAwesomeIconView(FontAwesomeIcon.REFRESH)) {
                        setOnAction {
                            decreeController.update()
                            decreeController2.update()
                        }
                    }
                    button("Добавить") {
                        setOnAction {
                            var d = t2.selectionModel.selectedItem
                            if (d != null && student.id != -1){
                                base.InsertStudentDecree(student.id,d.id)
                                decreeController.update()
                                decreeController2.update()
                            }
                        }
                    }
                    button("Убрать") {
                        setOnAction {
                            var d = t1.selectionModel.selectedItem
                            if (d != null && student.id != -1) {
                                base.DeleteStudentDecree(student.id, d.id)
                                decreeController.update()
                                decreeController2.update()
                            }
                        }
                    }
                }
                hbox {
                    t1 = tableview<Decree> {
                        items = decreeController.list
                        column("Название", Decree::name)
                        column("Тип", Decree::type)
                        column("Создан", Decree::dateCreate)
                        onUserSelect(2) {
                            var own = it.OpenEditor(con)
                        }
                    }
                    t2 = tableview<Decree> {
                        items = decreeController2.list
                        column("Название", Decree::name)
                        column("Тип", Decree::type)
                        column("Создан", Decree::dateCreate)
                        onUserSelect(2) {
                            var own = it.OpenEditor(con)
                        }
                    }
                }
            }
}
