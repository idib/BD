package StudentManager

import javafx.beans.property.ObjectProperty
import javafx.beans.value.ChangeListener
import javafx.scene.layout.Pane
import tornadofx.*
import tornadofx.Stylesheet.Companion.field
import java.time.LocalDate

/**
 * Created by idib on 31.05.17.
 */

class NullDate(name: String, switch:ObjectProperty<Boolean>, date:ObjectProperty<LocalDate?>) : Pane() {
    val root =
            field(name) {
                val usableDate = checkbox() {
                    bind(switch)
                }
                val dateSignatureField = datepicker() {
                    isDisable = true
                    setOnAction {
                        if (value != null)
                            date.value = value
                    }
                    if (switch.value) {
                        value = date.value
                        isDisable = false
                    }
                }
                usableDate.selectedProperty().addListener(ChangeListener { observable, oldValue, newValue ->
                    if (!usableDate.isSelected) {
                        dateSignatureField.isDisable = true
                        dateSignatureField.value = null
                        date.value = null
                    } else {
                        dateSignatureField.isDisable = false
                        dateSignatureField.value = LocalDate.now()
                    }
                })
            }
}