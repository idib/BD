package StudentManager

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.beans.value.ChangeListener
import javafx.scene.control.ComboBox
import javafx.scene.control.TableView
import javafx.scene.control.TextField
import javafx.scene.layout.Pane
import tornadofx.*
import kotlin.jdbc.BD

/**
 * Created by idib on 01.06.17.
 */

class doctab(var base: BD, var con: Controllers, var stud: StudentController) : Fragment() {
    var sor = stud.getNames()
    var names = sor.second
    var ids = sor.first
    var default: String = ""
    var documentController = con.doc
    var c1: ComboBox<String> by singleAssign()
    var student: Student = Student.nullStud(base)

    constructor(base: BD, con: Controllers) : this(base, con, con.stud) {
        stud.update()
        sor = stud.getNames()
        names = sor.second
        ids = sor.first
    }


    constructor(base: BD, con: Controllers, student: Student) : this(base, con, StudentController(base)) {
        documentController = DocumentController(base)
        stud.update(student.id)
        sor = stud.getNames()
        names = sor.second
        ids = sor.first
        c1.selectionModel.select(student.NameSur())
        this.student = student
        documentController.ii = student.id
    }

    override val root =
            vbox {
                hbox {
                    button("Добавить запись", FontAwesomeIconView(FontAwesomeIcon.PLUS)) {
                        setOnAction {
                            var d = Document.nullDoc(base)
                            if (student.id != -1)
                                d.idStud = student.id
                            var own = d.OpenEditor(con)
                        }
                    }
                    button("Обновить", FontAwesomeIconView(FontAwesomeIcon.REFRESH)) {
                        setOnAction {
                            con.update()
                        }
                    }
                    label("Выбрать по студенту")
                    c1 = combobox(values = names) {
                        valueProperty().addListener(ChangeListener { observable, oldValue, newValue ->
                            if (newValue != null && names.contains(newValue) && newValue != "") {
                                documentController.updateForStud(ids[names.indexOf(newValue)])

                            }
                            if (newValue != null && newValue == "") {
                                documentController.update()
                            }
                        })
                        selectionModel.select(default)
                    }
                }
                tableview<Document> {
                    items = documentController.list
                    column("Название", Document::name)
                    column("Тип", Document::type)
                    column("Создан", Document::dateCreate)
                    onUserSelect(2) {
                        var own = it.OpenEditor(con)
                    }
                }
            }
}

