package StudentManager

import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.control.TextField
import tornadofx.*
import kotlin.jdbc.BD

/**
 * Created by idib on 31.05.17.
 */
class Faculty(base: BD, id: Int, name: String) : Model() {
    override val id: Int by property(id)
    var name: String by property(name)
    val base: BD by property(base)

    companion object {
        fun nullFac(b: BD): Faculty {
            return Faculty(b, -1, "")
        }
    }

    fun nameProperty() = getProperty(Faculty::name)

    override fun CeateUpdate() {
        if (id == -1)
            base.InsertFaculty(this)
        else
            base.UpdateFaculty(this)
    }

    override fun Delete() {
        base.DeleteFaculty(this)
    }

    fun OpenEditor(con: Controllers) {
        val editScope = FacultyScope(this, con)
        return find(FacultyEditor::class, editScope).openWindow()
    }
}


class FacultyScope(var curModel: Faculty, var con: Controllers) : Scope()

class FacultyEditor() : View() {
    var nameField: TextField by singleAssign()
    val t = this
    val cmodel = (super.scope as FacultyScope).curModel
    val con = (super.scope as FacultyScope).con
    val obsList = con.stud

    override val root = form {
        fieldset("Факультет") {
            field("Название") {
                nameField = textfield {
                    bind(cmodel.nameProperty())
                }
            }
            children.addAll(SaveCanelDelete(cmodel, con, t))
        }
    }
}
