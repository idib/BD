package StudentManager

import ParentManager.Parent
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import tornadofx.Controller
import kotlin.jdbc.BD

/**
 * Created by idib on 01.06.17.
 */

class Controllers(var base: BD) : Controller() {
    var doc = DocumentController(base)
    var fac = FacultyController(base)
    var group = GroupController(base)
    var stud = StudentController(base)
    var treand = TreandController(base)
    var decree = DecreeController(base)
    var parent = ParentController(base)
    var typeDoc = typeDocController(base)
    var typeDec = typeDecController(base)

    open fun update() {
        doc.update()
        fac.update()
        group.update()
        stud.update()
        treand.update()
        decree.update()
        parent.update()
    }
}

class DocumentController(b: BD) {
    var list: ObservableList<Document> = FXCollections.observableArrayList()
    var base: BD = b
    var ii = -1
    var dd = -1

    fun update() {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.selectDocumentAll())
        if (ii != -1)
            updateForStud(ii)
        if (dd != -1)
            updateForStudUN(dd)
    }

    fun updateForStud(i: Int) {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.selectDocument(i))
    }

    fun updateForStudUN(i: Int) {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.selectDocumentUN(i))
    }
}

class FacultyController(b: BD) {
    var list: ObservableList<Faculty> = FXCollections.observableArrayList()
    var base: BD = b

    fun update() {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.selectFacultys())
    }
}

class GroupController(b: BD) {
    var list: ObservableList<Group> = FXCollections.observableArrayList()
    var base: BD = b

    fun update() {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.selectGroups())
    }

}

class StudentController(b: BD) {
    var list: ObservableList<Student> = FXCollections.observableArrayList()
    var base: BD = b

    constructor(b: BD, stud: Student) : this(b) {
        update(stud.id)
    }

    fun getNames(): Pair<List<Int>, List<String>> {
        var res = ArrayList<String>()
        var r = ArrayList<Int>()
        for (student in list) {
            res.add(student.NameSur())
            r.add(student.id)
        }
        res.add("")
        return Pair(r, res)
    }

    fun update(i: Int) {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.SelectStudent(i))
    }

    fun update() {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.SelectStudents())
    }
}

class TreandController(b: BD) {
    var list: ObservableList<Treand> = FXCollections.observableArrayList()
    var base: BD = b

    fun update() {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.selectTreands())
    }
}

class DecreeController(b: BD) {
    var list: ObservableList<Decree> = FXCollections.observableArrayList()
    var base: BD = b
    var ii = -1
    var dd = -1

    fun update() {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.selectDecreeAll())
        if (ii != -1)
            updateForStud(ii)
        if (dd != -1)
            updateForStudUN(dd)
    }

    fun updateForStud(i: Int) {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.selectDecree(i))
    }

    fun updateForStudUN(i: Int) {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.selectDecreeUN(i))
    }
}

class ParentController(b: BD) {
    var list: ObservableList<Parent> = FXCollections.observableArrayList()
    var base: BD = b
    var ii = -1
    var dd = -1

    fun update() {
        list.clear()
        list.addAll(base.selectParentAll())
        if (ii != -1)
            updateForStud(ii)
        if (dd != -1)
            updateForStudUN(dd)
    }

    fun updateForStud(i: Int) {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.selectParent(i))
    }

    fun updateForStudUN(i: Int) {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.selectParentUN(i))
    }
}

class typeDocController(b: BD) {
    var list: ObservableList<TypeDoc> = FXCollections.observableArrayList()
    var base: BD = b

    fun update() {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.selectTypeDoc())
    }
}

class typeDecController(b: BD) {
    var list: ObservableList<TypeDec> = FXCollections.observableArrayList()
    var base: BD = b

    fun update() {
        if (!list.isEmpty())
            list.clear()
        list.addAll(base.selectTypeDecree())
    }
}