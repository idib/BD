package StudentManager

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.beans.value.ChangeListener
import javafx.scene.control.ComboBox
import tornadofx.*
import kotlin.jdbc.BD

/**
 * Created by idib on 01.06.17.
 */

class studTab(base: BD, con: Controllers) : Fragment() {
    var c1: ComboBox<String> by singleAssign()
    var studentController = con.stud

    override val root =
            vbox {
                hbox {
                    button("Добавить запись", FontAwesomeIconView(FontAwesomeIcon.PLUS)) {
                        setOnAction {
                            var own = Student.nullStud(base).OpenEditor(con)
                        }
                    }
                    button("Обновить", FontAwesomeIconView(FontAwesomeIcon.REFRESH)) {
                        setOnAction {
                            studentController.update()
                        }
                    }
                }
                tableview<Student> {

                    studentController.list = items
                    studentController.update()


                    column("Фамилия", Student::surnameProperty)
                    column("Имя", Student::nameProperty)
                    column("Отчество", Student::patronymicProperty)
//                            column("Адресс", Student::adressProperty)
//                            column("Телефон", Student::phoneProperty)
//                            column("Проживание в общежитие", Student::dormProperty)
//                            column("Дата постапления", Student::dateEnrolmentProperty)
//                            column("Дата отчисления", Student::dateRemandProperty)
                    column("Группа", Student::groupProperty)
                    column("Направление", Student::treandProperty)
                    column("Факультет", Student::facultyProperty)

//                            column("Фамилия", Student::surname)
//                            column("Имя", Student::name)
//                            column("Отчество", Student::patronymic)
//                            column("Адресс", Student::adress)
//                            column("Телефон", Student::phone)
//                            column("Проживание в общежитие", Student::dorm)
//                            column("Дата постапления", Student::dateEnrolment)
//                            column("Дата отчисления", Student::dateRemand)
//                            column("Группа", Student::group)
//                            column("Направление", Student::treand)
//                            column("Факультет", Student::faculty)

                    onUserSelect(2, action = {
                        var own = it.OpenEditor(con)
                    })

//                            onUserSelect(editerStudent(it))
                }
            }
}
