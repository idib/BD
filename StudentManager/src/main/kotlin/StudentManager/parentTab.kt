package StudentManager

import ParentManager.Parent
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.beans.value.ChangeListener
import javafx.scene.control.ComboBox
import tornadofx.*
import kotlin.jdbc.BD

/**
 * Created by idib on 01.06.17.
 */

class parentTab(var base: BD, var con: Controllers, var stud: StudentController) : Fragment() {
    var sor = stud.getNames()
    var names = sor.second
    var ids = sor.first
    var default: String = ""
    var parentController = con.parent
    var c1: ComboBox<String> by singleAssign()
    var student: Student = Student.nullStud(base)

    constructor(base: BD, con: Controllers) : this(base, con, con.stud) {
        stud.update()
        sor = stud.getNames()
        names = sor.second
        ids = sor.first
    }


    constructor(base: BD, con: Controllers, student: Student) : this(base, con, StudentController(base)) {
        stud.update(student.id)
        sor = stud.getNames()
        names = sor.second
        ids = sor.first
        c1.selectionModel.select(student.NameSur())
        this.student = student
        parentController.ii = student.id
    }

    override val root =
            vbox {
                hbox {
                    button("Добавить запись", FontAwesomeIconView(FontAwesomeIcon.PLUS)) {
                        setOnAction {
                            var d = Parent.nullParent(base)
//                            if (student.id != -1)  todo
//                                d.idStud = student.id
                            var own = d.OpenEditor(con)
                        }
                    }
                    button("Обновить", FontAwesomeIconView(FontAwesomeIcon.REFRESH)) {
                        setOnAction {
                            con.update()
                        }
                    }
                    label("Выбрать по студенту")
                    c1 = combobox(values = names) {
                        valueProperty().addListener(ChangeListener { observable, oldValue, newValue ->
                            if (newValue != null && names.contains(newValue) && newValue != "") {
                                parentController.updateForStud(ids[names.indexOf(newValue)])

                            }
                            if (newValue != null && newValue == "") {
                                parentController.update()
                            }
                        })
                        selectionModel.select(default)
                    }
                }
                tableview<Parent> {
                    items = parentController.list

                    column("Фамилия", Parent::surnameProperty)
                    column("Имя", Parent::nameProperty)
                    column("Отчество", Parent::patronymicProperty)


                    onUserSelect(2, action = {
                        var own = it.OpenEditor(con)
                    })
                }
            }
}