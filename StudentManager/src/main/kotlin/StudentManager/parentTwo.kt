package StudentManager

import ParentManager.Parent
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.scene.control.TableView
import tornadofx.*
import kotlin.jdbc.BD

/**
 * Created by idib on 02.06.17.
 */

class parentTWO(var base: BD, var con: Controllers, var stud: StudentController) : Fragment() {
    var sor = stud.getNames()
    var names = sor.second
    var ids = sor.first
    var default: String = ""
    var parentController = ParentController(base)
    var parentController2 = ParentController(base)
    //    var c1: ComboBox<String> by singleAssign()
    var student: Student = Student.nullStud(base)
    var t1: TableView<Parent> by singleAssign()
    var t2: TableView<Parent> by singleAssign()

    constructor(base: BD, con: Controllers) : this(base, con, con.stud) {
        stud.update()
        sor = stud.getNames()
        names = sor.second
        ids = sor.first
    }


    constructor(base: BD, con: Controllers, student: Student) : this(base, con, StudentController(base)) {
        stud.update(student.id)
        sor = stud.getNames()
        names = sor.second
        ids = sor.first
//        c1.selectionModel.select(student.NameSur())
        this.student = student
        parentController.ii = student.id
        parentController2.dd = student.id
        parentController.update()
        parentController2.update()
    }

    override val root =
            vbox {
                hbox {
                    button("Добавить новую запись", FontAwesomeIconView(FontAwesomeIcon.PLUS)) {
                        setOnAction {
                            var d = Decree.nullDoc(base)
//                            if (student.id != -1)         todo
//                                d.idStud = student.id
                            var own = d.OpenEditor(con)
                        }
                    }
                    button("Обновить", FontAwesomeIconView(FontAwesomeIcon.REFRESH)) {
                        setOnAction {
                            parentController.update()
                            parentController2.update()
                        }
                    }
                    button("Добавить") {
                        setOnAction {
                            var d = t2.selectionModel.selectedItem
                            if (d != null && student.id != -1) {
                                base.InsertStudentParent(student.id, d.id)
                                parentController.update()
                                parentController2.update()
                            }
                        }
                    }
                    button("Убрать") {
                        setOnAction {
                            var d = t1.selectionModel.selectedItem
                            if (d != null && student.id != -1) {
                                base.DeleteStudentParent(student.id, d.id)
                                parentController.update()
                                parentController2.update()
                            }
                        }
                    }
                }
                hbox {
                    t1 = tableview<Parent> {
                        items = parentController.list
                        column("Фамилия", Parent::surname)
                        column("Имя", Parent::name)
                        column("Отчество", Parent::patronymic)
                        onUserSelect(2) {
                            var own = it.OpenEditor(con)
                        }
                    }
                    t2 = tableview<Parent> {
                        items = parentController2.list
                        column("Фамилия", Parent::surname)
                        column("Имя", Parent::name)
                        column("Отчество", Parent::patronymic)
                        onUserSelect(2) {
                            var own = it.OpenEditor(con)
                        }
                    }
                }
            }
}
