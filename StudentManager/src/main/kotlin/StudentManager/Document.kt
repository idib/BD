package StudentManager

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.beans.value.ChangeListener
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.control.CheckBox
import javafx.scene.control.ComboBox
import javafx.scene.control.DatePicker
import javafx.scene.control.TextField
import loginMenager.OpenUnitUni
import tornadofx.*
import java.time.LocalDate
import kotlin.jdbc.BD

/**
 * Created by idib on 31.05.17.
 */

class Document(var base: BD, override var id: Int, name: String,
               dateCreate: LocalDate, dateSgnature: LocalDate?,
               dateEntry: LocalDate?, dateEnd: LocalDate?,
               idStud: Int, idType: Int, type: String) : Model() {
    var name by property(name)
    var useSign by property(dateSgnature != null)
    var useEnt by property(dateEntry != null)
    var useEnd by property(dateEnd != null)
    var dateCreate by property(dateCreate)
    var dateSgnature by property(dateSgnature)
    var dateEntry by property(dateEntry)
    var dateEnd by property(dateEnd)
    var idStud by property(idStud)
    var idType by property(idType)
    var type by property(type)

    fun nameProperty() = getProperty(Document::name)
    fun dateCreateProperty() = getProperty(Document::dateCreate)
    fun useSignProperty() = getProperty(Document::useSign)
    fun useEntProperty() = getProperty(Document::useEnt)
    fun useEndProperty() = getProperty(Document::useEnd)
    fun dateSignatureProperty() = getProperty(Document::dateSgnature)
    fun dateEntProperty() = getProperty(Document::dateEntry)
    fun dateEndProperty() = getProperty(Document::dateEnd)

    override fun CeateUpdate() {
        if (id == -1)
            base.InsertDocument(this)
        else
            base.UpdateDocument(this)
    }

    override fun Delete() {
        base.DeleteDocument(this)
    }

    companion object {
        fun nullDoc(b: BD): Document {
            return Document(b, -1, "", LocalDate.now(), null, null, null, -1, -1, "")
        }
    }

    fun OpenEditor(con: Controllers) {
        val editScope = DocScope(this, con)
        return find(docEditor::class, editScope).openWindow()
    }
}


class DocScope(var curModel: Document, var con: Controllers) : Scope()


class docEditor : View() {
    var nameField: TextField by singleAssign()
    var dateCreateField: DatePicker by singleAssign()

    var usableDate: CheckBox by singleAssign()

    val cmodel = (super.scope as DocScope).curModel
    val con = (super.scope as DocScope).con
    val obsList = con.stud
    val stud = cmodel.base.SelectStudent(cmodel.idStud) // todo проверить на null и с типом документа тоже


    var c1: ComboBox<String> by singleAssign()

    var Tid: List<Int> = ArrayList()
    var Tlist: ObservableList<String> = FXCollections.observableArrayList()

    init {
        cmodel.base.selectTypeDoc(Tid, Tlist)
    }

    override val root = form {
        fieldset("Документ", FontAwesomeIconView(FontAwesomeIcon.PAPERCLIP)) {
            field("Название") {
                nameField = textfield {
                    bind(cmodel.nameProperty())
                }
            }

            field("Тип Документа") {
                c1 = combobox(values = Tlist) {
                    selectionModel.select(Tid.indexOf(cmodel.idType))
                    valueProperty().addListener(ChangeListener { observable, oldValue, newValue ->
                        if (newValue != null && Tlist.contains(newValue)) {
                            cmodel.idType = Tid[Tlist.indexOf(newValue)]
                            cmodel.type = c1.selectionModel.selectedItem
                        }
                    })
                }
            }

            field("Дата создания") {
                dateCreateField = datepicker() {
                    bind(cmodel.dateCreateProperty())
                }
            }

            children.addAll(NullDate("Дата подписания", cmodel.useSignProperty(), cmodel.dateSignatureProperty()))

            children.addAll(NullDate("Дата вступления в силу", cmodel.useEntProperty(), cmodel.dateEntProperty()))

            children.addAll(NullDate("Дата оканчания", cmodel.useEndProperty(), cmodel.dateEndProperty()))


            field("Прикреплен к студенту") {
                var s: String = ""
                if (stud != null) {
                    s = stud.NameSur()
                }
                label(s)
//                button("Подробности") {
//                    setOnAction {
//
//                    }
//                }
                button("Изменить") {
                    setOnAction {
                        OpenUnitUni(studTab(cmodel.base, con))
                    }
                }
            }

            children.addAll(SaveCanelDelete(cmodel, con, this@docEditor))
        }
    }
}