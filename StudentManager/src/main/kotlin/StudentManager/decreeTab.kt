package StudentManager

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.beans.value.ChangeListener
import javafx.scene.control.ComboBox
import tornadofx.*
import kotlin.jdbc.BD

/**
 * Created by idib on 01.06.17.
 */


class decreeTab(var base: BD, var con: Controllers, var stud: StudentController) : Fragment() {
    var sor = stud.getNames()
    var names = sor.second
    var ids = sor.first
    var default: String = ""
    var decreeController = con.decree
    var c1: ComboBox<String> by singleAssign()
    var student: Student = Student.nullStud(base)

    constructor(base: BD, con: Controllers) : this(base, con, con.stud) {
        stud.update()
        sor = stud.getNames()
        names = sor.second
        ids = sor.first
    }


    override val root =
            vbox {
                hbox {
                    button("Добавить запись", FontAwesomeIconView(FontAwesomeIcon.PLUS)) {
                        setOnAction {
                            var d = Decree.nullDoc(base)
//                            if (student.id != -1)  todo
//                                d.idStud = student.id
                            var own = d.OpenEditor(con)
                        }
                    }
                    button("Обновить", FontAwesomeIconView(FontAwesomeIcon.REFRESH)) {
                        setOnAction {
                            con.update()
                        }
                    }
                    label("Выбрать по студенту")
                    c1 = combobox(values = names) {
                        valueProperty().addListener(ChangeListener { observable, oldValue, newValue ->
                            if (newValue != null && names.contains(newValue) && newValue != "") {
                                decreeController.updateForStud(ids[names.indexOf(newValue)])

                            }
                            if (newValue != null && newValue == "") {
                                decreeController.update()
                            }
                        })
                        selectionModel.select(default)
                    }
                }
                tableview<Decree> {
                    items = decreeController.list
                    column("Название", Decree::name)
                    column("Тип", Decree::type)
                    column("Создан", Decree::dateCreate)
                    onUserSelect(2) {
                        var own = it.OpenEditor(con)
                    }
                }
            }
}