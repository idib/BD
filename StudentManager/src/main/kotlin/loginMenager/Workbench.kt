package loginMenager

/**
 * Created by idib on 19.05.17.
 */

import StudentManager.*
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.application.Platform
import javafx.scene.control.TabPane
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Pane
import javafx.scene.layout.VBox
import tornadofx.*
import tornadofx.Stylesheet.Companion.tab
import kotlin.jdbc.BD

class Workbench : View() {
    override val root = BorderPane()
    val loginController: LoginController by inject()
    val base: BD;

    init {
        base = BD()
        base.conect()
        val con = Controllers(base)
        val studentController = con.stud
        val groupController = con.group
        val treandController = con.treand
        val facultyController = con.fac
        val documentController = con.doc
        val decreeController = con.decree
        val parentController = con.parent
        val typeDocController = con.typeDoc
        val typeDecController = con.typeDec

        titleProperty.value = "Secure Workbench"
        with(root) {
            setPrefSize(800.0, 600.0)
            top {
                hbox {
                    button("Выйти из учетной записи") {
                        setOnAction {
                            loginController.logout()
                        }
                    }
                    button("Закрыть приложение") {
                        setOnAction {
                            base.close()
                            Platform.exit()
                        }
                    }
                }
            }
            center {
                tabpane {
                    tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE
                    tab("Студенты", VBox()) {
                        this += studTab(base, con)
                    }
                    tab("Факульты", VBox()) {
                        hbox {
                            button("Добавить запись", FontAwesomeIconView(FontAwesomeIcon.PLUS)) {
                                setOnAction {
                                    var own = Faculty.nullFac(base).OpenEditor(con)
                                }
                            }
                            button("Обновить", FontAwesomeIconView(FontAwesomeIcon.REFRESH)) {
                                setOnAction {
                                    facultyController.update()
                                }
                            }
                        }
                        tableview<Faculty> {
                            facultyController.list = items
                            facultyController.update()
                            column("Название", Faculty::name)
                            onUserSelect(2, action = {
                                var own = it.OpenEditor(con)
                            })
                        }
                    }
                    tab("Направления", VBox()) {
                        hbox {
                            button("Добавить запись", FontAwesomeIconView(FontAwesomeIcon.PLUS)) {
                                setOnAction {
                                    var own = Treand.nullTreand(base).OpenEditor(con)
                                }
                            }
                            button("Обновить", FontAwesomeIconView(FontAwesomeIcon.REFRESH)) {
                                setOnAction {
                                    treandController.update()
                                }
                            }
                        }
                        tableview<Treand> {
                            treandController.list = items
                            treandController.update()
                            column("Название", Treand::name)
                            column("Факультет", Treand::faculty)
                            onUserSelect(2, action = {
                                var own = it.OpenEditor(con)
                            })
                        }
                    }
                    tab("Группы", VBox()) {
                        hbox {
                            button("Добавить запись", FontAwesomeIconView(FontAwesomeIcon.PLUS)) {
                                setOnAction {
                                    var own = Group.nullGroup(base).OpenEditor(con)
                                }
                            }
                            button("Обновить", FontAwesomeIconView(FontAwesomeIcon.REFRESH)) {
                                setOnAction {
                                    groupController.update()
                                }
                            }
                        }
                        tableview<Group> {
                            groupController.list = items
                            groupController.update()
                            column("Название", Group::name)
                            column("Направление", Group::treand)
                            column("Факультет", Group::faculty)
                            onUserSelect(2, action = {
                                var own = it.OpenEditor(con)
                            })
                        }
                    }
                    tab("Документы", VBox()) {
                        this += doctab(base, con)
                    }
                    tab("Приказы", VBox()) {
                        this += decreeTab(base, con)
                    }
                    tab("Родители", VBox()) {
                        this += parentTab(base, con)
                    }
                    tab("Типы Документов", VBox()) {
                        hbox {
                            button("Добавить запись", FontAwesomeIconView(FontAwesomeIcon.PLUS)) {
                                setOnAction {
                                    var own = TypeDoc.nullTypeDoc(base).OpenEditor(con)
                                }
                            }
                            button("Обновить", FontAwesomeIconView(FontAwesomeIcon.REFRESH)) {
                                setOnAction {
                                    facultyController.update()
                                }
                            }
                        }
                        tableview<TypeDoc> {
                            items = typeDocController.list
                            typeDocController.update()
                            column("Название", TypeDoc::name)
                            onUserSelect(2, action = {
                                var own = it.OpenEditor(con)
                            })
                        }
                    }
                    tab("Типы Приказов", VBox()) {
                        hbox {
                            button("Добавить запись", FontAwesomeIconView(FontAwesomeIcon.PLUS)) {
                                setOnAction {
                                    var own = Faculty.nullFac(base).OpenEditor(con)
                                }
                            }
                            button("Обновить", FontAwesomeIconView(FontAwesomeIcon.REFRESH)) {
                                setOnAction {
                                    facultyController.update()
                                }
                            }
                        }
                        tableview<TypeDec> {
                            items = typeDecController.list
                            typeDecController.update()
                            column("Название", TypeDec::name)
                            onUserSelect(2, action = {
                                var own = it.OpenEditor(con)
                            })
                        }
                    }
                }
            }
        }
    }
}

public fun OpenUnitUni(f: Fragment) = UnitUni(f).openWindow()

class UnitUni(f: Fragment) : View() {
    override val root = vbox {
        this += f
    }
}