package kotlin.jdbc;

import ParentManager.Parent;
import StudentManager.*;
import javafx.collections.ObservableList;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by idib on 25.05.17.
 */
public class BD {
    static final String DB_URL = "jdbc:oracle:thin:@//localhost:1521/orcl";
    // JDBC driver name and database URL
    final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    //  Database credentials
    final String USER = "ilya";
    final String PASS = "1234";


    Connection conn = null;
    Statement stmt = null;
    String strDF = "yyyy-mm-dd";
    private DateTimeFormatter df = DateTimeFormatter.ofPattern(strDF);

    public BD() {

    }

    private boolean execute(String sql) throws SQLException {
        System.out.println(sql);
        boolean t = stmt.execute(sql);
//        System.out.println(t);
        return t;
    }

    private ResultSet executeQuery(String sql) throws SQLException {
        System.out.println(sql);
        ResultSet t = stmt.executeQuery(sql);
//        System.out.println(t);
        return t;
    }

    private int executeUpdate(String sql) throws SQLException {
        System.out.println(sql);
        int t = stmt.executeUpdate(sql);
//        System.out.println(t);
        return t;
    }

    public List<Student> SelectStudents() throws SQLException {
        String sql = "SELECT" +
                "  st.*," +
                "  GROUPS.PK_GROUP as idG," +
                "  GROUPS.NAME as Gname," +
                "  TREAND.NAME as Tname," +
                "  FACULTY.NAME as Fname" +
                " FROM STUDENT st" +
                "  JOIN GROUPS ON st.FK_GROUP = GROUPS.PK_GROUP" +
                "  JOIN TREAND ON GROUPS.FK_TREAND = TREAND.PK_TREND" +
                "  JOIN FACULTY ON TREAND.FK_FACULTY = FACULTY.PK_FACULTY";
        ResultSet rs = executeQuery(sql);

        List<Student> students = new ArrayList<>();

        while (rs.next()) {
            //Retrieve by column name
            int id = rs.getInt("PK_Student");
            String surname = rs.getString("Surname");
            String name = rs.getString("Name");
            String patronymic = rs.getString("Patronymic");
            String adress = rs.getString("Adress");
            String phone = rs.getString("Phone_Number");
            Boolean dorm = rs.getInt("Dorm") == 1;
            Date dateEnrolment = rs.getDate("Date_Enrolment");
            Date dateRemand = rs.getDate("Date_Remand");
            boolean isnul = dateRemand != null;
            int idG = rs.getInt("idG");
            String group = rs.getString("Gname");
            String treand = rs.getString("Tname");
            String faculty = rs.getString("Fname");


            LocalDate r = dateEnrolment.toLocalDate();
            LocalDate d;
            if (dateRemand != null)
                d = dateRemand.toLocalDate();
            else
                d = null;

            students.add(new Student(this, id, surname.trim(), name.trim(), patronymic.trim(),
                    adress.trim(), phone.trim(), dorm, r, isnul, d,
                    idG, group.trim(), treand.trim(), faculty.trim()));
        }
        rs.close();
        return students;
    }

    public Student SelectStudent(int idStud) throws SQLException {
        String sql = "SELECT" +
                "  st.*," +
                "  GROUPS.PK_GROUP as idG," +
                "  GROUPS.NAME as Gname," +
                "  TREAND.NAME as Tname," +
                "  FACULTY.NAME as Fname" +
                " FROM STUDENT st" +
                "  JOIN GROUPS ON st.FK_GROUP = GROUPS.PK_GROUP" +
                "  JOIN TREAND ON GROUPS.FK_TREAND = TREAND.PK_TREND" +
                "  JOIN FACULTY ON TREAND.FK_FACULTY = FACULTY.PK_FACULTY WHERE PK_STUDENT = " + idStud;
        ResultSet rs = executeQuery(sql);

        Student students = null;
        while (rs.next()) {
            //Retrieve by column name
            int id = rs.getInt("PK_Student");
            String surname = rs.getString("Surname");
            String name = rs.getString("Name");
            String patronymic = rs.getString("Patronymic");
            String adress = rs.getString("Adress");
            String phone = rs.getString("Phone_Number");
            Boolean dorm = rs.getInt("Dorm") == 1;
            Date dateEnrolment = rs.getDate("Date_Enrolment");
            Date dateRemand = rs.getDate("Date_Remand");
            boolean isnul = dateRemand != null;
            int idG = rs.getInt("idG");
            String group = rs.getString("Gname");
            String treand = rs.getString("Tname");
            String faculty = rs.getString("Fname");
            LocalDate r = dateEnrolment.toLocalDate();
            LocalDate d;
            if (dateRemand != null)
                d = dateRemand.toLocalDate();
            else
                d = null;

            students = (new Student(this, id, surname.trim(), name.trim(), patronymic.trim(),
                    adress.trim(), phone.trim(), dorm, r, isnul, d,
                    idG, group.trim(), treand.trim(), faculty.trim()));
            break;
        }
        rs.close();
        return students;
    }

    public void UpdateStudents(Student s) throws SQLException {
        String sql = "UPDATE STUDENT st" +
                " SET " +
                "  st.SURNAME        = \'" + s.getSurname() + "\'," +
                "  st.NAME           = \'" + s.getName() + "\'," +
                "  st.PATRONYMIC     = \'" + s.getPatronymic() + "\'," +
                "  st.ADRESS         = \'" + s.getAdress() + "\'," +
                "  st.PHONE_NUMBER   = \'" + s.getPhone() + "\'," +
                "  st.DORM           = " + (s.getDorm() ? 1 : 0) + "," +
                "  st.DATE_ENROLMENT = to_date(\'" + s.getDateEnrolment() + "\',\'YYYY-MM-DD\'),";
        sql += "  st.DATE_REMAND   = " +
                (s.getUseDate() ? " to_date(\'" + s.getDateRemand() + "\',\'YYYY-MM-DD\')," : "  null,");
        sql += "  st.FK_GROUP       = " + s.getIdGroup() +
                " WHERE st.PK_STUDENT = " + s.getId();
        executeUpdate(sql);
    }

    public void DeleteStudent(Student s) throws SQLException {
        String sql = "DELETE FROM STUDENT  WHERE PK_STUDENT = " + s.getId();
        execute(sql);
    }

    public void InsertStudent(Student s) throws SQLException {
        String sql = "INSERT INTO STUDENT (PK_STUDENT, SURNAME, NAME, PATRONYMIC, ADRESS, PHONE_NUMBER, DORM, DATE_ENROLMENT,";
        if (s.getDateRemand() != null)
            sql += " DATE_REMAND,";
        sql += " FK_GROUP)" +
                " VALUES ((SELECT max(PK_STUDENT) FROM STUDENT) + 1," +
                "\'" + s.getSurname() + "\',\'" + s.getName() + "\',\'" + s.getPatronymic() + "\',\'" + s.getAdress() + "\',\'" +
                s.getPhone() + "\'," + (s.getDorm() ? 1 : 0) + ", to_date('" + s.getDateEnrolment().toString() + "',\'YYYY-MM-DD\'),";

        if (s.getDateRemand() != null)
            sql += " to_date(\'" + s.getDateRemand().toString() + "\',\'YYYY-MM-DD\'),";
        sql += " " + s.getIdGroup() + "  )";
        executeUpdate(sql);
    }

    public int getTid(int id, List<Integer> r, ObservableList<String> t) throws SQLException {
        String sql = "SELECT *   FROM GROUPS g  WHERE g.FK_TREAND = (SELECT FK_TREAND FROM GROUPS r WHERE r.PK_GROUP = " + id + ")";
        ResultSet rs = executeQuery(sql);
        r.clear();
        t.clear();
        int idr = -1;
        while (rs.next()) {
            idr = rs.getInt("fk_treand");
            r.add(rs.getInt("pk_group"));
            t.add(rs.getString("name"));
        }
        rs.close();
        return idr;
    }

    public int getFid(int id, List<Integer> r, ObservableList<String> t) throws SQLException {

        String sql = "SELECT *   FROM TREAND g  WHERE g.FK_FACULTY = (SELECT FK_FACULTY FROM TREAND r WHERE r.PK_TREND = " + id + ")";
        ResultSet rs = executeQuery(sql);
        r.clear();
        t.clear();
        int idr = -1;
        while (rs.next()) {
            idr = rs.getInt("fk_faculty");
            r.add(rs.getInt("PK_TREND"));
            t.add(rs.getString("name"));
        }
        rs.close();
        return idr;
    }

    public void selectFacultys(List<Integer> i, List<String> s) throws SQLException {
        String sql = "SELECT *   FROM FACULTY g ";
        ResultSet rs = executeQuery(sql);
        i.clear();
        s.clear();
        while (rs.next()) {
            i.add(rs.getInt("pk_faculty"));
            s.add(rs.getString("name"));
        }
        rs.close();
    }

    public void getTreand(int id, List<Integer> r, ObservableList<String> t) throws SQLException {
        String sql = "SELECT *   FROM TREAND g  WHERE g.FK_FACULTY = " + id;
        System.out.println(sql);
        ResultSet rs = executeQuery(sql);
        r.clear();
        t.clear();
        while (rs.next()) {
            r.add(rs.getInt("pk_trend"));
            t.add(rs.getString("name"));
        }
        rs.close();
    }

    public void getGroup(int id, List<Integer> r, ObservableList<String> t) throws SQLException {

        String sql = "SELECT *   FROM GROUPS g  WHERE g.FK_TREAND = " + id;
        ResultSet rs = executeQuery(sql);
        r.clear();
        t.clear();
        while (rs.next()) {
            r.add(rs.getInt("pk_group"));
            t.add(rs.getString("name"));
        }
        rs.close();
    }

    public void close() {
        try {
            if (stmt != null)
                conn.close();
        } catch (SQLException se) {
        }// do nothing
        try {
            if (conn != null)
                conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }//end finally try
    }

    public void conect() throws ClassNotFoundException, SQLException {
        DriverManager.registerDriver(
                new oracle.jdbc.driver.OracleDriver());

        //STEP 3: Open a connection
        System.out.println("Connecting to a selected database...");
        conn = DriverManager.getConnection(DB_URL, USER, PASS);
        System.out.println("Connected database successfully...");

        //STEP 4: Execute a query
        System.out.println("Creating statement...");
        stmt = conn.createStatement();
    }

    public List<Faculty> selectFacultys() throws SQLException {
        String sql = "SELECT *   FROM FACULTY g ";
        ResultSet rs = executeQuery(sql);
        List<Faculty> res = new ArrayList();
        while (rs.next()) {
            res.add(new Faculty(this, rs.getInt("pk_faculty"), rs.getString("name")));
        }
        rs.close();
        return res;
    }

    public void InsertFaculty(Faculty faculty) throws SQLException {
        String sql = " INSERT INTO FACULTY (PK_FACULTY, NAME) " +
                " VALUES ((SELECT max(PK_FACULTY ) FROM FACULTY) + 1 ," +
                "\'" + faculty.getName() + "\')";
        executeUpdate(sql);
    }

    public void UpdateFaculty(Faculty faculty) throws SQLException {
        String sql = "UPDATE FACULTY " +
                " SET " +
                " NAME           = \'" + faculty.getName() + "\'" +
                " WHERE PK_FACULTY = " + faculty.getId();
        executeUpdate(sql);
    }

    public void DeleteFaculty(Faculty faculty) throws SQLException {
        String sql = "DELETE FROM FACULTY WHERE PK_FACULTY = " + faculty.getId();
        execute(sql);
    }

    public List<Treand> selectTreands() throws SQLException {
        String sql = "SELECT TREAND.*, FACULTY.NAME as fname  FROM TREAND LEFT JOIN  FACULTY on TREAND.FK_FACULTY = FACULTY.PK_FACULTY";
        ResultSet rs = executeQuery(sql);
        List<Treand> res = new ArrayList();
        while (rs.next()) {
            res.add(new Treand(this, rs.getInt("pk_trend"), rs.getString("name"),
                    rs.getInt("fk_faculty"), rs.getString("fname")));
        }
        rs.close();
        return res;
    }

    public void InsertTreand(Treand treand) throws SQLException {
        String sql = "INSERT INTO TREAND (PK_TREND, NAME, FK_FACULTY) " +
                " VALUES ((SELECT max(PK_TREND ) FROM TREAND) + 1 , " +
                "\'" + treand.getName() + "\', " +
                treand.getIdFaculty() + " )";
        executeUpdate(sql);
    }

    public void UpdateTreand(Treand treand) throws SQLException {
        String sql = "UPDATE TREAND " +
                " SET " +
                " NAME           = \'" + treand.getName() + "\'," +
                " FK_FACULTY           = \'" + treand.getIdFaculty() + "\'" +
                " WHERE PK_TREND = " + treand.getId();
        executeUpdate(sql);
    }

    public void DeleteTreand(Treand treand) throws SQLException {
        String sql = "DELETE FROM TREAND WHERE PK_TREND = " + treand.getId();
        execute(sql);
    }

    public List<Group> selectGroups() throws SQLException {
        String sql = "SELECT GROUPS.*, TREAND.NAME as tname, FACULTY.NAME as fname  FROM GROUPS JOIN TREAND on GROUPS.FK_TREAND = TREAND.PK_TREND JOIN FACULTY ON TREAND.FK_FACULTY = FACULTY.PK_FACULTY";
        ResultSet rs = executeQuery(sql);
        List<Group> res = new ArrayList();
        while (rs.next()) {
            res.add(new Group(this, rs.getInt("pk_group"), rs.getString("name"),
                    rs.getInt("fk_treand"), rs.getString("tname"), rs.getString("fname")));
        }
        rs.close();
        return res;
    }

    public void InsertGroup(Group group) throws SQLException {
        String sql = "INSERT INTO GROUPS (pk_group, NAME, fk_treand) " +
                " VALUES ((SELECT max(pk_group ) FROM GROUPS) + 1 , " +
                "\'" + group.getName() + "\', " +
                group.getIdTreand() + " )";
        executeUpdate(sql);
    }

    public void UpdateGroup(Group group) throws SQLException {
        String sql = "UPDATE GROUPS " +
                " SET " +
                " NAME           = \'" + group.getName() + "\'," +
                " fk_treand           = \'" + group.getIdTreand() + "\'" +
                " WHERE pk_group = " + group.getId();
        executeUpdate(sql);
    }

    public void DeleteGroup(Group group) throws SQLException {
        String sql = "DELETE FROM GROUPS WHERE pk_group = " + group.getId();
        execute(sql);
    }

    public List<Document> selectDocumentAll() throws SQLException {
        String sql = "SELECT DOCUNEMT.*,TYPE_DOCUMENT.NAME as tname FROM DOCUNEMT LEFT JOIN TYPE_DOCUMENT ON DOCUNEMT.FK_TYPE_DOCUMENT = TYPE_DOCUMENT.PK_TYPE_DOCUMENT";
        ResultSet rs = executeQuery(sql);
        List<Document> res = new ArrayList();
        while (rs.next()) {
            Date dateCreate = rs.getDate("date_create");
            Date dateSignature = rs.getDate("date_signature");
            Date dateEnt = rs.getDate("date_entry_into_force");
            Date dateEnd = rs.getDate("date_end_of_action");

            LocalDate dc = dateCreate.toLocalDate();
            LocalDate ds;
            LocalDate dent;
            LocalDate dend;

            ds = dateSignature != null ? dateSignature.toLocalDate() : null;
            dent = dateEnt != null ? dateEnt.toLocalDate() : null;
            dend = dateEnd != null ? dateEnd.toLocalDate() : null;


            res.add(new Document(this, rs.getInt("pk_document"), rs.getString("name"),
                    dc, ds, dent, dend, rs.getInt("fk_student"), rs.getInt("fk_type_document"),
                    rs.getString("tname")));
        }
        rs.close();
        return res;
    }

    public List<Document> selectDocument(int idStud) throws SQLException {
        String sql = "SELECT DOCUNEMT.*, TYPE_DOCUMENT.NAME AS tname FROM DOCUNEMT LEFT JOIN TYPE_DOCUMENT ON DOCUNEMT.FK_TYPE_DOCUMENT = TYPE_DOCUMENT.PK_TYPE_DOCUMENT WHERE FK_STUDENT = " +
                idStud;
        ResultSet rs = executeQuery(sql);
        List<Document> res = new ArrayList();
        while (rs.next()) {
            Date dateCreate = rs.getDate("date_create");
            Date dateSignature = rs.getDate("date_signature");
            Date dateEnt = rs.getDate("date_entry_into_force");
            Date dateEnd = rs.getDate("date_end_of_action");

            LocalDate dc = dateCreate.toLocalDate();
            LocalDate ds;
            LocalDate dent;
            LocalDate dend;

            ds = dateSignature != null ? dateSignature.toLocalDate() : null;
            dent = dateEnt != null ? dateEnt.toLocalDate() : null;
            dend = dateEnd != null ? dateEnd.toLocalDate() : null;


            res.add(new Document(this, rs.getInt("pk_document"), rs.getString("name"),
                    dc, ds, dent, dend, rs.getInt("fk_student"), rs.getInt("fk_type_document"),
                    rs.getString("tname")));
        }
        rs.close();
        return res;
    }

    public List<Document> selectDocumentUN(int idStud) throws SQLException {
        String sql = "SELECT DOCUNEMT.*, TYPE_DOCUMENT.NAME AS tname FROM DOCUNEMT LEFT JOIN TYPE_DOCUMENT ON DOCUNEMT.FK_TYPE_DOCUMENT = TYPE_DOCUMENT.PK_TYPE_DOCUMENT WHERE FK_STUDENT != " +
                idStud;
        ResultSet rs = executeQuery(sql);
        List<Document> res = new ArrayList();
        while (rs.next()) {
            Date dateCreate = rs.getDate("date_create");
            Date dateSignature = rs.getDate("date_signature");
            Date dateEnt = rs.getDate("date_entry_into_force");
            Date dateEnd = rs.getDate("date_end_of_action");

            LocalDate dc = dateCreate.toLocalDate();
            LocalDate ds;
            LocalDate dent;
            LocalDate dend;

            ds = dateSignature != null ? dateSignature.toLocalDate() : null;
            dent = dateEnt != null ? dateEnt.toLocalDate() : null;
            dend = dateEnd != null ? dateEnd.toLocalDate() : null;


            res.add(new Document(this, rs.getInt("pk_document"), rs.getString("name"),
                    dc, ds, dent, dend, rs.getInt("fk_student"), rs.getInt("fk_type_document"),
                    rs.getString("tname")));
        }
        rs.close();
        return res;
    }

    public void InsertDocument(Document document) throws SQLException {
        String sql = "INSERT INTO DOCUNEMT (PK_DOCUMENT, NAME, DATE_CREATE,";
        if (document.getUseSign())
            sql += " date_signature,";
        if (document.getUseEnd())
            sql += " date_end_of_action,";
        if (document.getUseEnt())
            sql += " date_entry_into_force,";
        sql += "fk_student, fk_type_document)" +
                " VALUES ((SELECT max(PK_STUDENT) FROM STUDENT) + 1," +
                "\'" + document.getName() + "\', to_date('" + document.getDateCreate() + "',\'YYYY-MM-DD\'),";
        if (document.getUseSign())
            sql += " to_date(\'" + document.getDateSgnature() + "\',\'YYYY-MM-DD\'),";
        if (document.getUseEnd())
            sql += " to_date(\'" + document.getDateEnd() + "\',\'YYYY-MM-DD\'),";
        if (document.getUseEnt())
            sql += " to_date(\'" + document.getDateEntry() + "\',\'YYYY-MM-DD\'), ";

        sql += document.getIdStud() + " , " +
                document.getIdType() + "  )";
        executeUpdate(sql);
    }

    public void UpdateDocument(Document document) throws SQLException {
        String sql = "UPDATE DOCUNEMT " +
                " SET " +
                " NAME           = \'" + document.getName() + "\', " +
                " fk_student      = \'" + document.getIdStud() + "\', " +
                " date_create    = to_date(\'" + document.getDateCreate() + "\',\'YYYY-MM-DD\'), " +
                " date_signature    = " +
                (document.getUseSign() ? " to_date(\'" + document.getDateSgnature() + "\',\'YYYY-MM-DD\')," : "  null,") +
                " date_entry_into_force    = " +
                (document.getUseEnt() ? " to_date(\'" + document.getDateEntry() + "\',\'YYYY-MM-DD\')," : "  null,") +
                " date_end_of_action   = " +
                (document.getUseEnd() ? " to_date(\'" + document.getDateEnd() + "\',\'YYYY-MM-DD\')," : "  null,") +
                " fk_type_document     = " + document.getIdType() +
                " WHERE PK_DOCUMENT    = " + document.getId();
        executeUpdate(sql);
    }

    public void DeleteDocument(Document document) throws SQLException {
        String sql = "DELETE FROM DOCUNEMT WHERE PK_DOCUMENT =" + document.getId();
        execute(sql);
    }

    public List<Decree> selectDecreeAll() throws SQLException {
        String sql = "SELECT DECREE.*, TYPE_DECREE.NAME as tname FROM DECREE LEFT JOIN TYPE_DECREE on DECREE.FK_TYPE_DECREE = TYPE_DECREE.PK_TYPE_DECREE";
        ResultSet rs = executeQuery(sql);
        List<Decree> res = new ArrayList();
        while (rs.next()) {
            Date dateCreate = rs.getDate("date_create");
            Date dateSignature = rs.getDate("date_signature");
            Date dateEnt = rs.getDate("date_entry_into_force");
            Date dateEnd = rs.getDate("date_end_of_action");

            LocalDate dc = dateCreate.toLocalDate();
            LocalDate ds;
            LocalDate dent;
            LocalDate dend;

            ds = dateSignature != null ? dateSignature.toLocalDate() : null;
            dent = dateEnt != null ? dateEnt.toLocalDate() : null;
            dend = dateEnd != null ? dateEnd.toLocalDate() : null;


            res.add(new Decree(this, rs.getInt("pk_decree"), rs.getString("name"),
                    dc, ds, dent, dend, rs.getInt("fk_type_decree"),
                    rs.getString("tname")));
        }
        rs.close();
        return res;
    }

    public List<Decree> selectDecree(int idStud) throws SQLException {
        String sql = "SELECT DECREE.*, TYPE_DECREE.NAME as tname FROM DECREE LEFT JOIN TYPE_DECREE on DECREE.FK_TYPE_DECREE = TYPE_DECREE.PK_TYPE_DECREE JOIN RELATION_9 ON DECREE.PK_DECREE = RELATION_9.FK_DECREE WHERE RELATION_9.FK_STUDENT = " +
                idStud;
        ResultSet rs = executeQuery(sql);
        List<Decree> res = new ArrayList();
        while (rs.next()) {
            Date dateCreate = rs.getDate("date_create");
            Date dateSignature = rs.getDate("date_signature");
            Date dateEnt = rs.getDate("date_entry_into_force");
            Date dateEnd = rs.getDate("date_end_of_action");

            LocalDate dc = dateCreate.toLocalDate();
            LocalDate ds;
            LocalDate dent;
            LocalDate dend;

            ds = dateSignature != null ? dateSignature.toLocalDate() : null;
            dent = dateEnt != null ? dateEnt.toLocalDate() : null;
            dend = dateEnd != null ? dateEnd.toLocalDate() : null;


            res.add(new Decree(this, rs.getInt("pk_decree"), rs.getString("name"),
                    dc, ds, dent, dend, rs.getInt("fk_type_decree"),
                    rs.getString("tname")));
        }
        rs.close();
        return res;
    }

    public List<Decree> selectDecreeUN(int idStud) throws SQLException {
        String sql = "SELECT DISTINCT\n" +
                "  DECREE.*,\n" +
                "  TYPE_DECREE.NAME AS tname\n" +
                "FROM DECREE\n" +
                "  LEFT JOIN TYPE_DECREE ON DECREE.FK_TYPE_DECREE = TYPE_DECREE.PK_TYPE_DECREE\n" +
                "WHERE\n" +
                "  NOT EXISTS(SELECT FK_DECREE\n" +
                "             FROM RELATION_9\n" +
                "             WHERE RELATION_9.FK_DECREE = DECREE.PK_DECREE AND RELATION_9.FK_STUDENT = " + idStud + " )";
        ResultSet rs = executeQuery(sql);
        List<Decree> res = new ArrayList();
        while (rs.next()) {
            Date dateCreate = rs.getDate("date_create");
            Date dateSignature = rs.getDate("date_signature");
            Date dateEnt = rs.getDate("date_entry_into_force");
            Date dateEnd = rs.getDate("date_end_of_action");

            LocalDate dc = dateCreate.toLocalDate();
            LocalDate ds;
            LocalDate dent;
            LocalDate dend;

            ds = dateSignature != null ? dateSignature.toLocalDate() : null;
            dent = dateEnt != null ? dateEnt.toLocalDate() : null;
            dend = dateEnd != null ? dateEnd.toLocalDate() : null;


            res.add(new Decree(this, rs.getInt("pk_decree"), rs.getString("name"),
                    dc, ds, dent, dend, rs.getInt("fk_type_decree"),
                    rs.getString("tname")));
        }
        rs.close();
        return res;
    }

    public void InsertDecree(Decree decree) throws SQLException {
        String sql = "INSERT INTO decree (pk_decree, NAME, DATE_CREATE,";
        if (decree.getUseSign())
            sql += " date_signature,";
        if (decree.getUseEnd())
            sql += " date_end_of_action,";
        if (decree.getUseEnt())
            sql += " date_entry_into_force,";
        sql += " fk_type_decree)" +
                " VALUES ((SELECT max(pk_decree) FROM decree) + 1," +
                "\'" + decree.getName() + "\', to_date('" + decree.getDateCreate() + "',\'YYYY-MM-DD\'),";
        if (decree.getUseSign())
            sql += " to_date(\'" + decree.getDateSgnature() + "\',\'YYYY-MM-DD\'),";
        if (decree.getUseEnd())
            sql += " to_date(\'" + decree.getDateEnd() + "\',\'YYYY-MM-DD\'),";
        if (decree.getUseEnt())
            sql += " to_date(\'" + decree.getDateEntry() + "\',\'YYYY-MM-DD\'), ";

        sql += decree.getIdType() + "  )";
        executeUpdate(sql);
    }

    public void UpdateDecree(Decree decree) throws SQLException {
        String sql = "UPDATE decree " +
                " SET " +
                " NAME           = \'" + decree.getName() + "\', " +
                " date_create    = to_date(\'" + decree.getDateCreate() + "\',\'YYYY-MM-DD\'), " +
                " date_signature    = " +
                (decree.getUseSign() ? " to_date(\'" + decree.getDateSgnature() + "\',\'YYYY-MM-DD\')," : "  null,") +
                " date_entry_into_force    = " +
                (decree.getUseEnt() ? " to_date(\'" + decree.getDateEntry() + "\',\'YYYY-MM-DD\')," : "  null,") +
                " date_end_of_action   = " +
                (decree.getUseEnd() ? " to_date(\'" + decree.getDateEnd() + "\',\'YYYY-MM-DD\')," : "  null,") +
                " fk_type_decree     = " + decree.getIdType() +
                " WHERE pk_decree    = " + decree.getId();
        executeUpdate(sql);
    }

    public void DeleteDecree(Decree decree) throws SQLException {
        String sql = "DELETE FROM decree WHERE pk_decree =" + decree.getId();
        execute(sql);
    }

    public void InsertStudentDecree(int idStud, int idDecr) throws SQLException {
        String sql = "INSERT INTO RELATION_9 (FK_DECREE, FK_STUDENT) VALUES (" + idDecr + " , " + idStud + " )";
        executeUpdate(sql);
    }

    public void DeleteStudentDecree(int idStud, int idDecr) throws SQLException {
        String sql = "DELETE FROM RELATION_9 WHERE FK_STUDENT = " + idStud + " and FK_DECREE = " + idDecr;
        execute(sql);
    }

    public List<Parent> selectParentAll() throws SQLException {
        String sql = "SELECT *\n" +
                "FROM PARENT";
        ResultSet rs = executeQuery(sql);
        List<Parent> res = new ArrayList();

        while (rs.next()) {
            //Retrieve by column name
            int id = rs.getInt("pk_parent");
            String surname = rs.getString("Surname");
            String name = rs.getString("Name");
            String patronymic = rs.getString("Patronymic");
            String adress = rs.getString("Adress");
            String phone = rs.getString("Phone_Number");

            res.add(new Parent(this, id, surname.trim(), name.trim(), patronymic.trim(),
                    adress.trim(), phone.trim()));
        }
        rs.close();
        return res;
    }

    public List<Parent> selectParent(int idStud) throws SQLException {
        String sql = "SELECT DISTINCT *\n" +
                "FROM PARENT\n" +
                "  JOIN RELATION_15 ON PARENT.PK_PARENT = RELATION_15.FK_PARENT\n" +
                "WHERE RELATION_15.FK_STUDENT = " + idStud;
        ResultSet rs = executeQuery(sql);
        List<Parent> res = new ArrayList();
        while (rs.next()) {
            int id = rs.getInt("pk_parent");
            String surname = rs.getString("Surname");
            String name = rs.getString("Name");
            String patronymic = rs.getString("Patronymic");
            String adress = rs.getString("Adress");
            String phone = rs.getString("Phone_Number");

            res.add(new Parent(this, id, surname.trim(), name.trim(), patronymic.trim(),
                    adress.trim(), phone.trim()));
        }
        rs.close();
        return res;
    }

    public List<Parent> selectParentUN(int idStud) throws SQLException {
        String sql = "SELECT DISTINCT\n" +
                "  *\n" +
                "FROM PARENT\n" +
                "WHERE\n" +
                "  NOT EXISTS(SELECT FK_PARENT\n" +
                "             FROM RELATION_15\n" +
                "             WHERE RELATION_15.FK_PARENT = PARENT.PK_PARENT AND RELATION_15.FK_STUDENT = " + idStud + ")";
        ResultSet rs = executeQuery(sql);
        List<Parent> res = new ArrayList();
        while (rs.next()) {
            int id = rs.getInt("pk_parent");
            String surname = rs.getString("Surname");
            String name = rs.getString("Name");
            String patronymic = rs.getString("Patronymic");
            String adress = rs.getString("Adress");
            String phone = rs.getString("Phone_Number");

            res.add(new Parent(this, id, surname.trim(), name.trim(), patronymic.trim(),
                    adress.trim(), phone.trim()));
        }
        rs.close();
        return res;
    }

    public void InsertParent(Parent parent) throws SQLException {
        String sql = "INSERT INTO PARENT (PK_PARENT, SURNAME, NAME, PATRONYMIC, ADRESS, PHONE_NUMBER)" +
                " VALUES ((SELECT max(PK_PARENT) FROM PARENT) + 1 ," +
                "\'" + parent.getSurname() + "\',\'" + parent.getName() + "\',\'" + parent.getPatronymic() + "\',\'" + parent.getAdress() + "\',\'" +
                parent.getPhone() + "\')";
        executeUpdate(sql);
    }

    public void UpdateParent(Parent parent) throws SQLException {
        String sql = "UPDATE parent st" +
                " SET " +
                "  st.SURNAME        = \'" + parent.getSurname() + "\'," +
                "  st.NAME           = \'" + parent.getName() + "\'," +
                "  st.PATRONYMIC     = \'" + parent.getPatronymic() + "\'," +
                "  st.ADRESS         = \'" + parent.getAdress() + "\'," +
                "  st.PHONE_NUMBER   = \'" + parent.getPhone() + "\'" +
                " WHERE st.PK_PARENT = " + parent.getId();
        executeUpdate(sql);
    }

    public void DeleteParent(Parent parent) throws SQLException {
        String sql = "DELETE FROM parent WHERE pk_parent =" + parent.getId();
        execute(sql);
    }

    public void InsertStudentParent(int idStud, int idPar) throws SQLException {
        String sql = "INSERT INTO RELATION_15 (FK_STUDENT, FK_PARENT) VALUES (" + idStud + " , " + idPar + " )";
        executeUpdate(sql);
    }

    public void DeleteStudentParent(int idStud, int idPar) throws SQLException {
        String sql = "DELETE FROM RELATION_15 WHERE FK_STUDENT = " + idStud + " and FK_PARENT = " + idPar;
        execute(sql);
    }

    public List<TypeDec> selectTypeDecree() throws SQLException {
        String sql = "SELECT * FROM TYPE_DECREE";
        ResultSet rs = executeQuery(sql);
        List<TypeDec> res = new ArrayList();
        while (rs.next()) {
            res.add(new TypeDec(this, rs.getInt("pk_type_decree"), rs.getString("name")));
        }
        rs.close();
        return res;
    }

    public void selectTypeDecree(List<Integer> tid, ObservableList<String> tlist) throws SQLException {
        String sql = "SELECT * FROM TYPE_DECREE";
        ResultSet rs = executeQuery(sql);
        tid.clear();
        tlist.clear();
        while (rs.next()) {
            tid.add(rs.getInt("pk_type_decree"));
            tlist.add(rs.getString("name"));
        }
        rs.close();
    }

    public void InsertTypeDecree(TypeDec faculty) throws SQLException {
        String sql = " INSERT INTO TYPE_DECREE (PK_TYPE_DECREE, NAME) " +
                " VALUES ((SELECT max(PK_TYPE_DECREE ) FROM TYPE_DECREE) + 1 ," +
                "\'" + faculty.getName() + "\')";
        executeUpdate(sql);
    }

    public void UpdateTypeDecree(TypeDec typeDoc) throws SQLException {
        String sql = "UPDATE TYPE_DECREE " +
                " SET " +
                " NAME           = \'" + typeDoc.getName() + "\'" +
                " WHERE PK_TYPE_DECREE = " + typeDoc.getId();
        executeUpdate(sql);
    }

    public void DeleteTypeDecree(TypeDec typeDoc) throws SQLException {
        String sql = "DELETE FROM TYPE_DECREE WHERE PK_TYPE_DECREE = " + typeDoc.getId();
        execute(sql);
    }

    public List<TypeDoc> selectTypeDoc() throws SQLException {
        String sql = "SELECT * FROM Type_document";
        ResultSet rs = executeQuery(sql);
        List<TypeDoc> res = new ArrayList();
        while (rs.next()) {
            res.add(new TypeDoc(this, rs.getInt("pk_type_document"), rs.getString("name")));
        }
        rs.close();
        return res;
    }

    public void selectTypeDoc(List<Integer> tid, ObservableList<String> tlist) throws SQLException {
        String sql = "SELECT * FROM Type_document";
        ResultSet rs = executeQuery(sql);
        tid.clear();
        tlist.clear();
        while (rs.next()) {
            tid.add(rs.getInt("pk_type_document"));
            tlist.add(rs.getString("name"));
        }
        rs.close();
    }

    public void InsertTypeDoc(TypeDoc faculty) throws SQLException {
        String sql = " INSERT INTO TYPE_DOCUMENT (PK_TYPE_DOCUMENT, NAME) " +
                " VALUES ((SELECT max(PK_TYPE_DOCUMENT ) FROM TYPE_DOCUMENT) + 1 ," +
                "\'" + faculty.getName() + "\')";
        executeUpdate(sql);
    }

    public void UpdateTypeDoc(TypeDoc typeDoc) throws SQLException {
        String sql = "UPDATE TYPE_DOCUMENT " +
                " SET " +
                " NAME           = \'" + typeDoc.getName() + "\'" +
                " WHERE PK_TYPE_DOCUMENT = " + typeDoc.getId();
        executeUpdate(sql);
    }

    public void DeleteTypeDoc(TypeDoc typeDoc) throws SQLException {
        String sql = "DELETE FROM TYPE_DOCUMENT WHERE PK_TYPE_DOCUMENT = " + typeDoc.getId();
        execute(sql);
    }
}