package temp

/**
 * Created by idib on 19.05.17.
 */
import javafx.application.Application
import loginMenager.Styles
import tornadofx.App
import tornadofx.importStylesheet

class CustomerApp : App() {
    override val primaryView = CustomerForm::class

    init {
        importStylesheet(Styles::class)
    }
}

fun main(args: Array<String>) {
    Application.launch(CustomerApp::class.java, *args)
}